const request = require('supertest')

const app = require('./api')

describe('Dummy test', () => {

  afterEach(async () => {
    const connectDb = require('./db')
    const dbConnection = await connectDb()
    dbConnection.collection('users').removeMany({})
  })

  it('GET /', async () => {
    const res = await request(app).get('/')

    const { status, text } = res

    expect(status).toEqual(200)
    expect(JSON.parse(text)).toEqual({ message: 'aupa ahi' })
  })

  describe('GET /users', () => {
    it('Returns created users', async () => {
      const payload1 = {
        username: 'User1',
        email: 'user@domain.com',
        password: 'mypassword'
      }
      const payload2 = {
        username: 'User2',
        email: 'user@domain.com',
        password: 'mypassword'
      }
      await request(app).post('/users').send(payload1)
      await request(app).post('/users').send(payload2)

      const res = await request(app).get('/users')

      const { status, text } = res
      const body = JSON.parse(text)
      expect(status).toEqual(200)
      expect(body).toHaveLength(2)
      expect(body[0]).toHaveProperty('username', 'User1' )
      expect(body[1]).toHaveProperty('username', 'User2' )
    })
  })

  describe('POST /users', () => {
    it('Returns 200 if everything goes ok', async () => {
      const payload = {
        username: 'User',
        email: 'user@domain.com',
        password: 'mypassword'
      }

      const res = await request(app).post('/users').send(payload)

      const { status, text } = res
      const body = JSON.parse(text)
      expect(status).toEqual(200)
      expect(body).toHaveProperty('message', 'user created' )
      expect(body).toHaveProperty('id')
    })

    it('Cannot create users with the same username', async () => {
      const payload = {
        username: 'User',
        email: 'user@domain.com',
        password: 'mypassword'
      }
      await request(app).post('/users').send(payload)

      const res = await request(app).post('/users').send(payload)

      const { status, text } = res
      const body = JSON.parse(text)
      expect(status).toEqual(400)
      expect(body).toHaveProperty('errors')
      expect(body.errors).toHaveLength(1)
      expect(body.errors[0]).toBe('username already in use')
    })

    it('Cannot create users with passwords shorter than 8 characters', async () => {
      const payload = {
        username: 'User',
        email: 'user@domain.com',
        password: '123'
      }

      const res = await request(app).post('/users').send(payload)

      const { status, text } = res
      const body = JSON.parse(text)
      expect(status).toEqual(400)
      expect(body).toHaveProperty('errors')
      expect(body.errors).toHaveLength(1)
      expect(body.errors[0]).toBe('password must have 8 or more characters')
    })

    it('Cannot create users with an invalid email', async () => {
      const payload = {
        username: 'User',
        email: 'user',
        password: 'mypassword'
      }

      const res = await request(app).post('/users').send(payload)

      const { status, text } = res
      const body = JSON.parse(text)
      expect(status).toEqual(400)
      expect(body).toHaveProperty('errors')
      expect(body.errors).toHaveLength(1)
      expect(body.errors[0]).toBe('email not valid')
    })

    it('Response includes all validation errors', async () => {
      const payload = {
        username: 'User',
        email: 'user',
        password: '123'
      }

      const res = await request(app).post('/users').send(payload)

      const { status, text } = res
      const body = JSON.parse(text)
      expect(status).toEqual(400)
      expect(body).toHaveProperty('errors')
      expect(body.errors).toHaveLength(2)
      expect(body.errors).toEqual(
        expect.arrayContaining([
          'password must have 8 or more characters',
          'email not valid'
        ])
      )
    })
  })

})
