const express = require('express')

const getDb = require('./db')

const app = express()
app.use(express.json())

app.get('/', (req, res) => {
  return res.json({ message: 'aupa ahi' })
})

app.get('/samples', async (req, res) => {
  console.log('GET /samples')

  const db = await getDb()
  const cursor = db.collection('samples').find()
  const items = await cursor.toArray()
  return res.json({ items })
})

app.post('/samples', async (req, res) => {
  console.log('POST /samples', req.body)

  const db = await getDb()
  const {
    insertedCount,
    insertedId,
    result
  } = await db.collection('samples').insertOne(req.body)
  return res.json({ insertedCount, insertedId, result })
})

app.delete('/samples', async (req, res) => {
  console.log('DELETE /samples')

  const db = await getDb()
  const { deletedCount, result } = await db.collection('samples').deleteMany({})

  return res.json({ deletedCount, result })
})

app.post('/users', async (req, res) => {
  console.log('POST /users')

  const errors = []

  if (!req.body.password || typeof req.body.password !== 'string' || req.body.password.length < 8) {
    errors.push('password must have 8 or more characters')
  }

  if (!req.body.email || typeof req.body.email !== 'string' || !req.body.email.includes('@') || !req.body.email.includes('.')) {
    errors.push('email not valid')
  }

  if (errors.length) {
    return res.status(400).json({ errors })
  }

  const db = await getDb()

  const user = await db.collection('users').findOne({username: req.body.username})
  if (user) {
    return res.status(400).json({ errors: ['username already in use'] })
  }

  const { insertedId } = await db.collection('users').insertOne(req.body)

  const text = {'message': 'user created', 'id': insertedId}
  return res.json(text)
})

app.get('/users', async (req, res) => {
  console.log('GET /users')

  const db = await getDb()
  const cursor = await db.collection('users').find()
  const users = await cursor.toArray()
  return res.json(users)
})

module.exports = app
